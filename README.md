# Redirector

A Browser Extension for Chromium based browsers like Brave, Chromium, Google Chrome, MS Edge, ...
The Extension allows to specify domains/urls that should be redirected somewhere else.
E.g. redirect google.com to duckduckgo or brave search
or redirect youtube to your invidious instance
You can set up if you want to also redirect the path and parameters -> yt search will be a search on inviodous else it will only redirect to the starting page.

<img src=".github/images/redirector.png" alt="redirector image" width="400"/>

the extension saves the settings in the sync storage -> if you have other browsers synced e.g. via brave sync the addon config 
will also sync between them if addon sync is turned on in the settings.

## Installation
### Webstore
Comming Soon ... currently only manual installation is working sry
It is planned to add it to the chrome webstore in a short time 

### Manual Instalation
clone the git repository
``` git clone https://codeberg.org/raphiel/Chrome-Redirect-Extension.git ```

allow the developer mode in your browser in the extensions tab

load unpacked extension and select the src folder from this repository

## Examples
domain: www.google.com
redirection: https://search.brave.com
append_path: true
get_parameter: true 
-> auto redirect all google searches to brave

## About
Redirector is a free and opensource project
If you want to support this project you can make an improvement and make an pull request

if you find a bug or have a feature request just open an issue ...


## Credits
#### Icons
<a href="https://www.flaticon.com/free-icons/random" title="random icons">Random icons created by Freepik - Flaticon</a>

#### Style Inspiration
<a href=https://codepen.io/MoritzGiessmann/pen/PobErJB>MoritzGiessmann - Codepen</a>