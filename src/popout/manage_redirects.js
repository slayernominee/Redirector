var storage = chrome.storage.sync;

function set_redirects(redirects) {
    // write the redirects in the synced storage
    storage.set({ "redirects": redirects }, function(){});
}

function reset_redirects() {
    // clear the redirects in the synced storage
    set_redirects({});
}

function set_enabled(enabled) {
    // set the enabled value in synced storage
    storage.set({ "enabled": enabled }, function(){});
}

function list_redirects(redirects) {
    document.getElementById('redirects_list').innerHTML = '';
    for (var prop in redirects) {
        document.getElementById('redirects_list').innerHTML += `
<details id="details_prop_${prop}">
    <summary>
        ${prop}
    </summary>
    <ul>
        <li>to: ${redirects[prop]['redirect_to']}</li>
        <li>append path: ${redirects[prop]['append_path']}</li>
        <li>get parameter: ${redirects[prop]['get_parameter']}</li>
        <br>
        <button class="delete_prop_button" id="delete_prop_button_${prop}">delete</button>
    </ul>
</details>
        `;
    }
}

function delete_property(domain) {
    storage.get(["redirects"], function(items){
        var redirects = (items['redirects']);
        
        // delete the redirect
        delete redirects[domain];
        set_redirects(redirects);
        list_redirects(redirects);

    });

    document.getElementById('details_prop_' + domain).style.display = 'none';
}

function delete_button_lister_activate() {
    delete_prop_buttons = Array.from(document.getElementsByClassName("delete_prop_button"));
    delete_prop_buttons.forEach(btn => {
        btn.addEventListener('click', function(event) {
            src = event.srcElement;
            src_id = src.id;
            domain = src_id.split('delete_prop_button_')[1];
            delete_property(domain);
        });
    });
}

document.addEventListener('DOMContentLoaded', function() {
    storage.get(["redirects"], function(items){
        var redirects = (items['redirects']);
        list_redirects(redirects);
        delete_button_lister_activate(redirects);
    });
    var enabled;
    toogle_extension_button = document.getElementById('toogle_extension');
    try {
        storage.get(["enabled"], function(items) {
            
            if (items["enabled"] === undefined) {
                set_enabled(true);
                enabled = true;
                toogle_extension_button.checked = true;
            } else {
                enabled = Boolean(items["enabled"]);
                toogle_extension_button.checked = enabled;
            }
        });
    } catch (error) {
        set_enabled(true);
        enabled = true;
        toogle_extension_button.checked = true;
    }

    
    toogle_extension_button.addEventListener('click', function() {
        enabled = !enabled;
        storage.set({ "enabled": enabled }, function(){
            //  A data saved callback omg so fancy
        });
        toogle_extension_button = document.getElementById('toogle_extension');
        toogle_extension_button.checked = enabled;
    }, false);

    
    // reset button
    var reset_redirects = document.getElementById('reset_redirects');
    reset_redirects.addEventListener('click', function() {
        reset_redirects();
        list_redirects(redirect_default);
    }, false);

    var add_redirect = document.getElementById('add_redirect');
    var create_button = document.getElementById('create_button');

    add_redirect.addEventListener('click', function() {
        document.getElementById('add_redirect_inp').style.display = 'block';
        document.getElementById('create_button').style.display = 'block';
        add_redirect.style.display = 'none';
    }, false);

    create_button.addEventListener('click', function() {
        domain = document.getElementById('inp_add_redirect_domain').value;
        redirection = document.getElementById('inp_add_redirect_redirection').value;

        if (domain === '' || redirection === '') {
            alert('require a domain and redirection url!');
            return false;
        } else if (domain.indexOf('.') === -1) {
            alert('please enter a valid domain like e.g. www.domain.com');
            return false;
        } else if (redirection.indexOf('.') === -1 || redirection.indexOf('http') === -1) {
            alert('please enter a valid redirection url e.g. https://search.brave.com')
            return false;
        }

        path = document.getElementById('inp_add_redirect_path').value;
        parameter = document.getElementById('inp_add_redirect_parameter').value;

        storage.get(["redirects"], function(items){
            var redirects = (items['redirects']);
            if (redirects === undefined) {
                redirects = {
                    
                };
            }
    
            if (redirects[domain] === undefined) {
                redirects[domain] = {
                    "redirect_to": redirection,
                    "append_path": path,
                    "get_parameter": parameter
                }
                set_redirects(redirects);
                list_redirects(redirects);

                document.getElementById('add_redirect_inp').style.display = 'none';
                document.getElementById('create_button').style.display = 'none';
                add_redirect.style.display = 'block';

                alert('you might need to relaod your current tab to make it work...');
            } else {
                alert('cancled the creation becasue for this domain already exists a redirect! Please delete this before trying to create a new one for this domain');
            }
            
        });

        
        

        
    }, false);
}, false);

