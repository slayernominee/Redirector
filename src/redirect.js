// first check if the extension is enabled
chrome.storage.sync.get(['enabled'], function(items) {
    if (items['enabled'] === true) {
        var url = window.location.href;
        var hostname = window.location.hostname;
        
        // load the redirect settings
        chrome.storage.sync.get(["redirects"], function(items){
            var redirects = (items['redirects']);
            
            if (hostname in redirects) {
                // redirect
                redirect_options = redirects[hostname];
                redirect_to = redirect_options['redirect_to']; 
                if (Boolean(redirect_options['append_path'])) {
                    path = url.split(hostname + '/')[1].split('?')[0];
                    if (redirect_to.endsWith("/")) {
                        redirect_to = redirect_to + path;
                    } else {
                        redirect_to = redirect_to + '/' + path;
                    }
                }
                if (Boolean(redirect_options['get_parameter'])) {
                    parameter = url.split('?')[1]; 
                    redirect_to = redirect_to + '?' + parameter;
                }
                window.location = redirect_to;
            } else {
                // check for embeds
                var iframes = document.getElementsByTagName('iframe'); 
                
                // TODO: check if the iframes match an domain and if they match apply the changes to the src links
                // append_path and get_parameter needs also to be used

                /*
                for (frame in iframes) {
                    // TODO: get hostname
                    frame_hostname = 'youtube.com';
                    if (frame_hostname in reidrects) {
                        frame_redirect_options = redirects[hostname];
                        frame_redirect_to = frame_redirect_options['redirect_to'];

                        // append path and get parameter needs to be used

                        // set the iframe src to the frame_redirect_to

                    }
                }
                */

            }
        });
    }
});


